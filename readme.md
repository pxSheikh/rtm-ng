
# Project Name: RTM-NG

## Introduction

**RTM-NG** is an advanced Python bot designed to detect and execute arbitrage opportunities across centralized exchanges (CEXs). 
> Developed in 2018, this bot automates the trading process, ensuring efficient and profitable transactions between different exchanges.

## Project Overview

- **Inception Year:** 2018
- **Type:** Python Bot
- **Usage:** CEX Arbitrage Opportunity Detection & Execution

## Configuration

1. **API Keys:**
	- Configure your API keys in `conf/api-keys.conf` according to `api-keys.template.conf`.

2. **Adding New Exchanges:**
	- To add a new exchange, create the necessary files in `conf/markets/` and `api/`.

3. **Settings:**
	- The `settings.conf` file allows you to configure transaction sizes and expected profits.

4. **Database Creation:**
	- Run the command `./create-logdb.sh` to create the database for transaction history and logs.

5. **Execution:**
	- Run the command `./run.sh` to execute a single run of the RTM-NG bot.

		**Optional parameters:**
		- `-n`: _Prevents automatic_ `git pull` _execution_
		- `-l [time]`: _Initiates a loop that runs the bot cyclically at specified intervals (in seconds)_
		- `-t [time]`: _Allows the bot to restart immediately after each transaction at specified delay (in seconds)_

## Features

- _**Multi-CEX Support:** Easily add and configure multiple exchanges._
- _**Auto Trading:** Fully automated trading from detection to execution._
- _**Custom Settings:** Adjust currencies, pairs, transaction sizes, and profit expectations via `.conf` files._
- _**Scalability:** Designed to handle multiple exchanges, trading pairs, and large volumes of data efficiently._

## Tech Stack

- **Python:** _Core programming language used for the bot's logic_
- **REST API:** _Fetch market data and execute trades_
- **SQL:** _Transaction history and logs_
- **Libraries:** json, requests, threads, sqlite3, math, time, importlib
