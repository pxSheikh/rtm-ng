class Trade:
	""" Abstract trade class """
	def __init__(self, m1, m2, product):
		self.m1 = m1
		self.m2 = m2
		self.product = product
		self.trade = "TRADE "

		self._profit = self.calc()

	def __str__(self):
		c1, c2 = self.product.split("_")
		return "[%s][  %s  ][%s_%s] profit: \t%20.10f %s" % (self.trade, c1, self.m1.code(), self.m2.code(), self.profit(), c2)

	def calc(self):
		return None

	def profit(self):
		return self._profit


class DirectTrade(Trade):
	""" Calculate direct trade profits """
	def __init__(self, m1, m2, product):
		Trade.__init__(self, m1, m2, product)
		self.trade = "DIRECT"

	def calc(self):
		return self.m2.commission_sell(self.m2.bid(self.product)) - self.m1.commission_buy(self.m1.ask(self.product))


class EuroTrade(Trade):
	""" Calculate euro trade profits """
	def __init__(self, m1, m2, product1, product2):
		self.product1 = product1
		self.product2 = product2

		Trade.__init__(self, m1, m2, None)
		self.trade = " EURO "

	def __str__(self):
		c1, c2 = self.product1.split("_")
		return "[%s][  %s  ][%s_%s][BUY ]   \t%20.10f\n[%s][  %s  ][%s_%s][SELL]   \t%20.10f" % (
			self.trade, c1, self.m1.code(), self.m2.code(), self.profit()["buy"],
			self.trade, c1, self.m1.code(), self.m2.code(), self.profit()["sell"]
		)

	def buy(self):
		""" Kupuje w EUR """
		return self.m1.commission_sell(self.m1.bid(self.product1)) / self.m2.commission_buy(self.m2.ask(self.product2))

	def sell(self):
		""" Sprzedaje w EUR """
		return self.m1.commission_buy(self.m1.ask(self.product1)) / self.m2.commission_sell(self.m2.bid(self.product2))

	def calc(self):
		return {"buy": self.buy(), "sell": self.sell()}


class CryptoTrade(Trade):
	""" Calculate crypto trade profits """
	def __init__(self, m1, m2, product1, product2, product3):
		self.product1 = product1
		self.product2 = product2
		self.product3 = product3

		Trade.__init__(self, m1, m2, None)
		self.trade = "CRYPTO"

	def __str__(self):

		return "[%s][%s][%s_%s][BUY ]   \t%20.10f\n[%s][%s][%s_%s][SELL]   \t%20.10f" % (
			self.trade, self.product1, self.m1.code(), self.m2.code(), self.profit()["buy"],
			self.trade, self.product1, self.m1.code(), self.m2.code(), self.profit()["sell"],
		)

	def buy(self):
		return self.m2.commission_sell(
			self.m2.bid(self.product2)
		) - self.m2.commission_buy(
			self.m1.commission_buy(
				self.m1.ask(self.product1)
			)
		) * self.m2.ask(self.product3)

	def sell(self):
		return self.m2.commission_sell(
			self.m1.commission_sell(
				self.m1.bid(self.product1)
			) * self.m2.bid(self.product3)
		) - self.m2.commission_buy(
			self.m2.ask(self.product2)
		)

	def calc(self):
		return {
			"buy": self.buy(),
			"sell": self.sell()
		}


class CrazyCryptoTrade(CryptoTrade):
	def __init__(self, m1, m2, m3, product1, product2, product3):
		self.m3 = m3

		CryptoTrade.__init__(self, m1, m2, product1, product2, product3)
		self.trade = "CRAZY "

	def __str__(self):

		return "[%s][%s/%s/%s][%s_%s_%s][BUY ]   \t%20.10f\n[%s][%s/%s/%s][%s_%s_%s][SELL]   \t%20.10f" % (
			self.trade, self.product1, self.product2, self.product3, self.m1.code(), self.m2.code(), self.m3.code(), self.profit()["buy"],
			self.trade, self.product1, self.product2, self.product3, self.m1.code(), self.m2.code(), self.m3.code(), self.profit()["sell"],
		)

	def buy(self):
		return self.m2.commission_sell(
			self.m2.bid(self.product2)
		) - self.m3.commission_buy(
			self.m1.commission_buy(
				self.m1.ask(self.product1)
			)
		) * self.m3.ask(self.product3)

	def sell(self):
		return self.m3.commission_sell(
			self.m1.commission_sell(
				self.m1.bid(self.product1)
			) * self.m3.bid(self.product3)
		) - self.m2.commission_buy(
			self.m2.ask(self.product2)
		)


def maketrades(mm, settings):
	trades = {
		"direct": [],
		"euro": [],
		"crypto": [],
		"crazy": []
	}
	eurotrade_check = []

	for m1, _m1 in mm.all().iteritems():
		for m2, _m2 in mm.all().iteritems():
			if m1 != m2:
				for p1 in _m1.fetched_products():
					for p2 in _m2.fetched_products():
						# direct trade
						if p1 == p2:
							if "direct" not in settings["trade_blacklist"]:
								t = DirectTrade(_m1, _m2, p1)
								trades["direct"].append(t)
								if settings["verbose"]:
									print str(t)
						else:
							p1_1, p1_2 = p1.split("_")
							p2_1, p2_2 = p2.split("_")
							if p1_1 == p2_1:
								# euro trade
								if "euro" not in settings["trade_blacklist"]:
									if p1_2 == "PLN" and p2_2 == "EUR":
										if not "%s/%s/%s/%s" % (m1, m2, p1, p2) in eurotrade_check:
											t = EuroTrade(_m1, _m2, p1, p2)
											trades["euro"].append(t)
											if settings["verbose"]:
												print str(t)
											eurotrade_check.append("%s/%s/%s/%s" % (m1, m2, p1, p2))

									elif p1_2 == "EUR" and p2_2 == "PLN":
										if not "%s/%s/%s/%s" % (m2, m1, p2, p1) in eurotrade_check:
											t = EuroTrade(_m2, _m1, p2, p1)
											trades["euro"].append(t)
											if settings["verbose"]:
												print str(t)
											eurotrade_check.append("%s/%s/%s/%s" % (m2, m1, p2, p1))
								# crypto trade
								if "crypto" not in settings["trade_blacklist"]:
									for p3 in _m2.fetched_products():
										p3_1, p3_2 = p3.split("_")
										if p2_2 == p3_2 and p3_1 == p1_2:
											t = CryptoTrade(_m1, _m2, p1, p2, p3)
											trades["crypto"].append(t)
											if settings["verbose"]:
												print str(t)
								# crazy crypto trade
								if "crazy" not in settings["trade_blacklist"]:
									for m3, _m3 in mm.all().iteritems():
										if m2 == m3:
											continue  # skip cryptotrade when matching crazy
										for p3 in _m3.fetched_products():
											p3_1, p3_2 = p3.split("_")
											if p2_2 == p3_2 and p3_1 == p1_2:
												t = CrazyCryptoTrade(_m1, _m2, _m3, p1, p2, p3)
												trades["crazy"].append(t)
												if settings["verbose"]:
													print str(t)
	return trades
