import sys
import json
from time import time
from datetime import datetime

import trade
import action
import display
from market import MarketManager
from threads import ThreadWrapper
from log import Log


def main(settings_path):
	""" Main procedure """
	totalstart = time()
	print "\n* RTM-NG 0.9.0.5 * ~REVIVE *\n"
	print "[ ToDo ] -> proxied requests"
	print "[ ToDo ] -> APIs & conf/ update"
	print "[ ToDo ] -> Python3 migration\n"

	# loading
	loadstart = time()
	with open(settings_path) as f:
		settings = json.load(f)

	log = Log(settings)
	log.start_run()

	mm = MarketManager(settings)
	mm.load_all()
	loaddiff = time() - loadstart

	# data gathering
	datastart = time()
	# create threads
	thread_pool = {}
	for market, m in mm.all().iteritems():
		thread_pool[market] = {}
		for product in m.products():
			thread_pool[market][product] = ThreadWrapper(
				target=m.update_ask_bid,
				args=(
					product,
					settings["sums"][product.split("_")[0]])
			)
		thread_pool[market]["BALANCES"] = ThreadWrapper(target=m.update_balances)

	only_crypto_exr = False
	cached_exr = log.load_exchange_rates()
	if cached_exr:
		mm.load_exchange_rates(cached_exr)
		only_crypto_exr = True
	exchange_rates_thr = ThreadWrapper(target=mm.update_exchange_rates, kwargs={"only_crypto": only_crypto_exr})

	# start threads
	for m, pool in thread_pool.iteritems():
		for p, thread in pool.iteritems():
			thread.start()
	exchange_rates_thr.start()
	print

	# join threads
	for market, pool in thread_pool.iteritems():
		for product, thread in pool.iteritems():
			thread.join()
			if thread.error:
				print "[%s][%s] ERROR: %s" % (market, product, str(thread.error))
				continue

			if product == "BALANCES":
				for c, b in mm.get(market).balances().iteritems():
					log.log_balance("before", market, c, b)
				mm.get(market).print_balances()
			else:
				log.log_price(market, product, mm.get(market).ask(product), mm.get(market).bid(product))

	exchange_rates_thr.join()
	if exchange_rates_thr.error:
		print "[EXCHANGE RATES] ERROR: %s" % str(exchange_rates_thr.error)
	else:
		if not only_crypto_exr:
			log.log_exchange_rates(mm)
	datadiff = time() - datastart

	# calculation
	calcstart = time()
	print
	trades = trade.maketrades(mm, settings)
	print "[RTM] %d trade options: %d direct, %d euro, %d crypto, %d crazy." % (
		len(trades["direct"]) + len(trades["euro"]) * 2 + len(trades["crypto"]) * 2 + len(trades["crazy"]) * 2,
		len(trades["direct"]),
		len(trades["euro"]) * 2,		# single eurotrade contains buy and sell trades
		len(trades["crypto"]) * 2,		# single cryptotrade contains buy and sell trades
		len(trades["crazy"]) * 2		# single crazytrade contains buy and sell trades
	)
	print "\n\n\n"
	transactions, skipped = action.calculate_actions(trades, settings, mm)

	display.pretty_print_skipped(settings, skipped)
	for t in transactions:
		print "%s\n" % str(t)
	calcdiff = time() - calcstart

	# actions
	actionstart = time()

	t = action.pick_transaction(transactions, settings)
	print "======================== PICKED TRANSACTION ========================"
	print str(t)
	print "===================================================================="
	if t:
		t.run()
		t.finish()
		log.log_trade(t)
	actiondiff = time() - actionstart

	# update balances
	data2start = time()
	if t:
		thread_pool = {}
		for market, m in mm.all().iteritems():
			thread_pool[market] = ThreadWrapper(target=m.update_balances)

		for m in thread_pool:
			thread_pool[m].start()

		for m in thread_pool:
			thread_pool[m].join()
			if thread_pool[m].error:
				print "[%s][BALANCES] ERROR: %s" % (m, str(thread_pool[m].error))
				continue

			for c, b in mm.get(m).balances().iteritems():
				log.log_balance("after", m, c, b)

	display.pretty_print_all_balances(settings, mm, 4)
	data2diff = time() - data2start

	totaldiff = time() - totalstart

	log.finish_run(loaddiff, datadiff + data2diff, calcdiff, actiondiff, totaldiff, bool(t))
	print "\n\n\
		Load time:\t\t%fs\n\
		Data time:\t\t%fs\n\
		Calculation time:\t%fs\n\
		Action time:\t\t%fs\n\
		Total time:\t\t%fs\n\
		Current time:\t\t%s" \
		% (loaddiff, datadiff + data2diff, calcdiff, actiondiff, totaldiff, str(datetime.now()))

	return 1 if t else 0


if __name__ == "__main__":
	sys.exit(main(sys.argv[1] if len(sys.argv) >= 2 else "conf/settings.conf"))
