BEGIN TRANSACTION;
CREATE TABLE "transactions" (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	`runid`	INTEGER NOT NULL,
	`timestamp`	REAL NOT NULL,
	`date`	TEXT NOT NULL,
	`time`	TEXT NOT NULL,
	`type`	TEXT NOT NULL,
	`profit`	REAL NOT NULL,
	`currency`	TEXT NOT NULL,
	`normalized_profit`	REAL NOT NULL,
	`percent_profit`	REAL NOT NULL,
	`percent_base`	TEXT NOT NULL,
	`market1`	TEXT NOT NULL,
	`market2`	TEXT NOT NULL,
	`currencies`	TEXT NOT NULL,
	`actions`	INTEGER NOT NULL
);
CREATE TABLE "trades" (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	`runid`	INTEGER NOT NULL,
	`timestamp`	REAL NOT NULL,
	`date`	TEXT NOT NULL,
	`time`	TEXT NOT NULL,
	`market`	TEXT NOT NULL,
	`product`	NUMERIC NOT NULL,
	`type`	TEXT NOT NULL,
	`amount`	REAL NOT NULL,
	`currency`	TEXT NOT NULL,
	`for_amount`	REAL NOT NULL,
	`for_currency`	TEXT NOT NULL,
	`response`	TEXT
);
CREATE TABLE "run" (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	`timestamp`	REAL NOT NULL,
	`load_time`	REAL,
	`data_time`	REAL,
	`calc_time`	REAL,
	`action_time`	REAL,
	`total_time`	REAL,
	`active`	INTEGER
);
CREATE TABLE "prices" (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	`runid`	INTEGER NOT NULL,
	`timestamp`	REAL NOT NULL,
	`market`	TEXT NOT NULL,
	`product`	TEXT NOT NULL,
	`ask`	REAL NOT NULL,
	`bid`	REAL NOT NULL
);
CREATE TABLE `exchange_rates` (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	`runid`	INTEGER NOT NULL,
	`timestamp`	REAL NOT NULL,
	`data`	TEXT NOT NULL
);
CREATE TABLE `errors` (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	`runid`	INTEGER NOT NULL,
	`timestamp`	REAL NOT NULL,
	`type`	TEXT NOT NULL,
	`message`	TEXT NOT NULL
);
CREATE TABLE "balances" (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	`runid`	INTEGER NOT NULL,
	`timestamp`	REAL NOT NULL,
	`time`	TEXT NOT NULL,
	`market`	TEXT NOT NULL,
	`currency`	TEXT NOT NULL,
	`balance`	REAL NOT NULL
);
COMMIT;
