import json
import importlib
from time import time
from glob import glob


class Market:
	""" Market class. Supports loading market config and common market operations """
	def __init__(self, app_conf, config):
		self.app_conf = app_conf
		self.conf = None
		self.api = None
		self._balances = None
		self._prices = {}

		self.load(config)

	def load(self, config):
		with open(config) as cf:
			lines = [line for line in cf.readlines() if not line.strip().startswith("#")]
			self.conf = json.loads("".join(lines))

		if "api" in self.conf:
			self.api = importlib.import_module("api.%s" % self.conf["api"]["api"])

		if "test_mode" in self.conf:
			self.conf["test_mode"] = self.app_conf["test_mode"] or self.conf["test_mode"]
		else:
			self.conf["test_mode"] = self.app_conf["test_mode"]

	def name(self):
		return self.conf["name"]

	def code(self):
		return self.conf["code"]

	def test_mode(self):
		return self.conf["test_mode"]

	def version(self):
		return self.conf["version"]

	def products(self):
		return self.conf["products"].keys()

	def hold(self, currency):
		try:
			return self.conf["hold"][currency]
		except KeyError:
			# print "[RTM-MARKET][%s] No hold level defined for %s." % (self.code(), currency)
			return 0

	def fetched_products(self):
		return self._prices.keys()

	def ask(self, product):
		return self._prices[product]["ask"]

	def bid(self, product):
		return self._prices[product]["bid"]

	def __str__(self):
		return "[%s] %s %s" % (self.code(), self.name(), self.version())

	def update_ask_bid(self, product, _sum):
		""" Get ask and bid prices for given product and sum """
		start = time()
		# print "[%s][%s] Updating..." % (self.code(), product)

		if self.api is None:
			print "[RTM-MARKET][%s] No API." % self.code()
			return 0

		ob = self.api.orderbook(self.conf, product)
		ask, bid, _, _ = self._get_ask_bid(ob, _sum)
		self._prices[product] = {
			"ask": ask,
			"bid": bid
		}
		print "[%s][%s]\t\task: %20.10f\tbid: %20.10f" % (self.code(), product, ask, bid)
		end = time()
		return end - start

	def update_balances(self):
		if self.api:
			print "[%s][BALANCES] Updating..." % (self.code(),)
			self._balances = self.api.balances(self.conf)

	def print_balances(self):
		if self._balances:
			print "[%s][BALANCES]\t\n%s" % (
				self.code(),
				"\n".join(map(lambda k: "%20.10f %s" % (float(self._balances[k]), k), self._balances.keys()))
			)

	def balance(self, currency):
		if self._balances and currency in self._balances:
			return float(self._balances[currency])
		else:
			return -1

	def balances(self):
		return self._balances

	def commission_buy(self, value):
		""" Account value for market commission """
		return ((100 + self.conf["commission"]["buy"]) / 100.0) * float(value)

	def crypto_commission_buy(self, value):
		""" Subtract commission from value """
		return ((100 - self.conf["commission"]["buy"]) / 100.0) * float(value)

	def commission_sell(self, value):
		""" Subtract commission from value """
		return ((100 - self.conf["commission"]["sell"]) / 100.0) * float(value)

	def crypto_commission_sell(self, value):
		return self.commission_sell(value)

	def _get_ask_bid(self, data, _sum):
		""" Get ask and bid for sum
			This function expects data to be an orderbook data in the following format:
			data = {
				"asks": [
					[price, amount, ...],
					...
				],
				"bids": [
					[price, amount, ...],
					...
				]
			}
			where asks are sorted by price in ascending order and
			bids are sorted by price in descending order.
		"""
		p_ask, e_ask = self._get_price(data["asks"], _sum)
		p_bid, e_bid = self._get_price(data["bids"], _sum)

		return p_ask, p_bid, e_ask, e_bid

	def _get_price(self, data, amount):
		""" Get price for data sum """
		_sum = 0
		_entries = []
		if not data:
			print "[RTM-MARKET][%s] Empty orderbook." % self.code()
			raise Exception("Empty orderbook.")
		for entry in data:
			_entries.append((float(entry[1]), float(entry[0])))
			_sum += float(entry[1])
			if _sum >= amount:
				return float(entry[0]), _entries
		else:
			print "[RTM-MARKET][%s] Sum failed." % self.code()
			raise Exception("Sum failed.")

	def buy(self, product, amount, for_v):
		if self.api:
			return self.api.buy(self.conf, product, amount, for_v)
		else:
			print "[RTM-MARKET][%s] No API." % self.code()

	def sell(self, product, amount, for_v):
		if self.api:
			return self.api.sell(self.conf, product, amount, for_v)
		else:
			print "[RTM-MARKET][%s] No API." % self.code()


class MarketManager:
	""" Manager class for markets """
	def __init__(self, conf):
		self.conf = conf
		self._markets = {}
		self.exr_api = importlib.import_module("api.%s" % self.conf["exchange_rates"]["api"])
		self.crr_api = importlib.import_module("api.%s" % self.conf["crypto_rates"]["api"])
		self._exchange_rates = None

	def load_all(self):
		for cf in glob("%s/*.conf" % self.conf["markets_path"]):
			m = Market(self.conf, cf)
			if m.code() in self.conf["market_blacklist"]:
				print "LOAD: [BLACKLISTED]%s, not loaded" % str(m)
				continue
			if m.test_mode():
				print "LOAD: [TEST MODE]%s" % str(m)
			else:
				print "LOAD: %s" % str(m)
			self._markets[m.code()] = m
		print

	def get(self, code):
		return self._markets[code]

	def all(self):
		return self._markets

	def update_exchange_rates(self, only_crypto=False):
		print "[EXCHANGE RATES] Updating..."
		if only_crypto:
			self._exchange_rates = self._exchange_rates or {}
		else:
			self._exchange_rates = self.exr_api.exchange_rates(self.conf)
		self._exchange_rates.update(self.crr_api.exchange_rates(self.conf))
		print self.exchange_str()

	def exchange_str(self):
		return "\n".join(["[EXCHANGE RATE][%s] %20.10f" % (k, v) for k, v in self._exchange_rates.iteritems()])

	def exchange_rate(self, currency):
		if currency in self._exchange_rates:
			return self._exchange_rates[currency]
		elif currency == self.conf["base_currency"]:
			return 1
		else:
			print "[RTM-MARKETMANAGER] Exchange rate %s/%s not found." % (currency, self.conf["base_currency"])
			return 0

	def dump_exchange_rates(self):
		return json.dumps(self._exchange_rates)

	def load_exchange_rates(self, data):
		self._exchange_rates = json.loads(data)
		print self.exchange_str()
