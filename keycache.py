import json


class KeyCache:
	cache = {}

	@staticmethod
	def get_keyfile(file_path):
		if file_path in KeyCache.cache.keys():
			print "[RTM-KEYCACHE] %s, hit." % file_path
			return KeyCache.cache[file_path]
		else:
			print "[RTM-KEYCACHE] %s, miss." % file_path
			with open(file_path) as f:
				key_data = json.load(f)
			KeyCache.cache[file_path] = key_data
			return key_data

	@staticmethod
	def clear():
		print "[RTM-KEYCACHE] Cache cleared."
		KeyCache.cache = {}
