import math

from action import TransactionType


def pretty_print_all_balances(settings, mm, in_line):
	print

	all_currencies = []
	all_balances = {}
	for m in mm.all().values():
		all_balances[m.code()] = m.balances()
		if not all_balances[m.code()]:
			del all_balances[m.code()]
		else:
			for currency in all_balances[m.code()]:
				if currency not in all_currencies:
					all_currencies.append(currency)

	total = {}
	for currency in all_currencies:
		total[currency] = {}
		total[currency]['total'] = 0

	for exchange, balance in all_balances.iteritems():
		for asset, value in balance.iteritems():
			total[asset][exchange] = value
			total[asset]['total'] += float(value)

	curr_list = sorted(total.keys())
	for currency in ('PAX', 'USDT', 'USDC', 'IDR', 'PLN', 'EUR'):
		if currency in curr_list:
			curr_list.remove(currency)
			curr_list.append(currency)

	for asset in curr_list:
		markets = total[asset]

		# Hide No 0 Balances
		if markets['total'] == 0:
			continue

		cmc = mm.exchange_rate(asset)

		markets_chain = '|'.join('   [{0:4}]: {1:13.5f} {2}   '.format(
			exchange,
			float(value),
			asset[0] + asset[2:4] if len(asset) > 3 else asset
		) for exchange, value in markets.iteritems() if exchange != 'total')
		markets = markets_chain.split('|')

		lines = int(math.ceil(len(markets) / float(in_line)))

		print '[{0:^7.{1}f}]   *   {2:<12}     |{3}'.format(
			cmc,
			3 if cmc < 10 else 0 if cmc > 10000 else 1 if cmc > 100 and cmc < 1000 else 2,
			'{0:.2f} {1}'.format(cmc * total[asset]['total'], settings["base_currency"]),
			'|'.join(markets[:in_line])
		)
		if lines < 2:
			print '[{0:^7}]   ~   {1:<12.{2}f}'.format(
				asset[0] + asset[2:4] if len(asset) > 3 else asset,
				total[asset]['total'],
				2 if total[asset]['total'] > 100 else 6 if total[asset]['total'] < 10 else 5
			)
		elif lines > 1:
			print '[{0:^7}]   ~   {1:<12.{2}f}     |{3}'.format(
				asset[0] + asset[2:4] if len(asset) > 3 else asset,
				total[asset]['total'],
				2 if total[asset]['total'] > 100 else 6 if total[asset]['total'] < 10 else 5,
				'|'.join(markets[in_line:2 * in_line])
			)
			if lines > 2:
				current_line = 2
				for line in range(lines - 2):
					print '{0:33}|{1}'.format(
						'',
						'|'.join(markets[in_line * current_line:in_line * (current_line + 1)])
					)
					current_line += 1
		print


def pretty_print_skipped(settings, skipped):
	direct = {}
	euro = {}
	crypto = {}
	crazy = {}

	best = {"direct": {}, "euro": {}, "crypto": {}, "crazy": {}}

	def get_profit(settings, t):
		if settings["bestskipped_mode"] == "value":
			return t.profit
		elif settings["bestskipped_mode"] == "normalized":
			return t.normalized_profit()
		elif settings["bestskipped_mode"] == "percent":
			return t.percent_profit()

	def print_entry(t, type_selector, side_selector=None):
		if side_selector is None:
			best_t = best[type_selector][t.product]["transaction"]
		else:
			best_t = best[type_selector][t.product][side_selector]["transaction"]

		if t.type == TransactionType.Direct or t.type == TransactionType.Crypto or t.type == TransactionType.Crazy:
			try:
			    threshold = settings["threshold"]["custom"][type_selector][t.product]
			except KeyError:
			    try:
			    	threshold = settings["threshold"]["custom"][type_selector][t.product.split("_")[0]]
			    except KeyError:
			    	threshold = settings['threshold']['default'][type_selector]
		elif t.type == TransactionType.Euro:
			threshold = settings["threshold"][type_selector][side_selector]
		else:
			threshold = "!!UNKNOWN!!"

		if t.skipped_by == "profit":
			print "%17s[%s] SKIPPED, profit too low %s\t(trading on %s)\t\t%s" % (
				"",
				"%s_%s_%s" % (t.actions[0].market.code(), t.actions[1].market.code(), t.actions[2].market.code())
				if t.type == TransactionType.Crazy else
				"%s_%s" % (t.actions[0].market.code(), t.actions[-1].market.code()),
				t.format_profit_too_low(threshold),
				t.currencies_str(),
				"<-- BEST!" if best_t == t else ""
			)
		elif t.skipped_by == "funds":
			print "%17s[%s] SKIPPED, funds or hold level %s\t(trading on %s)\t\t%s" % (
				"",
				"%s_%s_%s" % (t.actions[0].market.code(), t.actions[1].market.code(), t.actions[2].market.code())
				if t.type == TransactionType.Crazy else
				"%s_%s" % (t.actions[0].market.code(), t.actions[-1].market.code()),
				t.format_profit(threshold),
				t.currencies_str(),
				"<-- BEST!" if best_t == t else ""
			)

	def handle_double_t(t, tlist, selector):
		if t.product not in tlist.keys():
			tlist[t.product] = {}
			best[selector][t.product] = {}
		if t.additional_info not in tlist[t.product].keys():
			tlist[t.product][t.additional_info] = []
			best[selector][t.product][t.additional_info] = {"transaction": None, "profit": 0}
		tlist[t.product][t.additional_info].append(t)

		profit = get_profit(settings, t)
		if best[selector][t.product][t.additional_info]["profit"] < profit:
			best[selector][t.product][t.additional_info]["transaction"] = t
			best[selector][t.product][t.additional_info]["profit"] = profit

	for t in skipped:
		if t.type == TransactionType.Direct:
			if t.product not in direct.keys():
				direct[t.product] = []
				best["direct"][t.product] = {"transaction": None, "profit": 0}
			direct[t.product].append(t)

			profit = get_profit(settings, t)
			if best["direct"][t.product]["profit"] < profit:
				best["direct"][t.product]["transaction"] = t
				best["direct"][t.product]["profit"] = profit

		if t.type == TransactionType.Euro:
			handle_double_t(t, euro, "euro")

		if t.type == TransactionType.Crypto:
			handle_double_t(t, crypto, "crypto")

		if t.type == TransactionType.Crazy:
			handle_double_t(t, crazy, "crazy")

	for product, tlist in direct.iteritems():
		print "[!ACTION][DIRECT][%s]" % product
		for t in tlist:
			print_entry(t, "direct")
		print

	for product, tdict in euro.iteritems():
		if "buy" in tdict.keys():
			print "[!ACTION][ EURO ][%s][BUY ]" % product
			for t in tdict["buy"]:
				print_entry(t, "euro", "buy")
			print
		if "sell" in tdict.keys():
			print "[!ACTION][ EURO ][%s][SELL]" % product
			for t in tdict["sell"]:
				print_entry(t, "euro", "sell")
			print

	for product, tdict in crypto.iteritems():
		if "buy" in tdict.keys():
			print "[!ACTION][CRYPTO][%s][BUY ]" % product
			for t in tdict["buy"]:
				print_entry(t, "crypto", "buy")
			print
		if "sell" in tdict.keys():
			print "[!ACTION][CRYPTO][%s][SELL]" % product
			for t in tdict["sell"]:
				print_entry(t, "crypto", "sell")
			print

	for product, tdict in crazy.iteritems():
		if "buy" in tdict.keys():
			print "[!ACTION][CRAZY ][%s][BUY ]" % product
			for t in tdict["buy"]:
				print_entry(t, "crazy", "buy")
			print
		if "sell" in tdict.keys():
			print "[!ACTION][CRAZY ][%s][SELL]" % product
			for t in tdict["sell"]:
				print_entry(t, "crazy", "sell")
			print
