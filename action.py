import json
from time import sleep

from threads import ThreadWrapper


class ActionType:
	Buy = 0
	Sell = 1


class TransactionType:
	Direct = 0
	Euro = 1
	Crypto = 2
	Crazy = 3


class Action:
	def __init__(self, market, type, product, currency, amount, for_c, for_v, prefix="\t"):
		self.market = market
		self.type = type
		self.product = product
		self.currency = currency
		self.amount = amount
		self.for_c = for_c
		self.for_v = for_v
		self.prefix = prefix
		self.thread = None
		self.valid = False

	def __str__(self):
		return "%s[%s][on %s][%s][%s] %20.10f\t~ [%s] %20.10f" % (
			self.prefix,
			self.market.code(),
			self.product,
			self.buysell_str(),
			self.currency,
			self.amount,
			self.for_c,
			self.for_v
		)

	def buysell_str(self):
		if self.type == ActionType.Buy:
			return "BUY "
		elif self.type == ActionType.Sell:
			return "SELL"

	def run(self):
		if self.type == ActionType.Buy:
			target = self.market.buy
		elif self.type == ActionType.Sell:
			target = self.market.sell
		self.thread = ThreadWrapper(
			target=target,
			args=(
				self.product,
				self.amount,
				self.for_v
			)
		)
		self.thread.start()

	def finish(self):
		self.thread.join()
		if self.thread.error:
			print self.thread.error
		print self.thread.result
		self.result = self.thread.result

	def check(self, transaction_type):
		if self.type == ActionType.Buy:
			hold = 0 if transaction_type == TransactionType.Crypto else self.market.hold(self.for_c)
			if self.market.balance(self.for_c) - self.for_v >= hold:
				self.valid = True
				return True
		elif self.type == ActionType.Sell:
			hold = 0 if transaction_type == TransactionType.Crypto else self.market.hold(self.currency)
			if self.market.balance(self.currency) - self.amount >= hold:
				self.valid = True
				return True
		self.valid = False
		return False

	def dump_http(self):
		return json.dumps({
			"rq": {
				"headers": dict([(k, str(v)) for k, v in self.result[0].iteritems()]),
				"body": self.result[1]
			},
			"rs": {
				"headers": dict([(k, str(v)) for k, v in self.result[2].iteritems()]),
				"body": self.result[3]
			}
		})


class Transaction:
	def __init__(self, type, product, profit, profit_currency, conf, mm, prefix="\t", additional_info=None):
		self.type = type
		self.product = product
		self.profit = profit
		self.profit_currency = profit_currency
		self.prefix = prefix
		self.additional_info = additional_info
		self.actions = []
		self.actions_valid = []
		self.valid = False
		self.conf = conf
		self.mm = mm

	def __str__(self):
		actions_str = []
		for action in self.actions:
			actions_str.append(str(action))

		if self.type == TransactionType.Euro:
			return "[+ACTION][%s]\n%s\n%sEXCHANGE RATE: %20.10f\n[-ACTION]" % (
				self.type_str(),
				"\n".join(actions_str),
				self.prefix,
				self.profit
			)
		else:
			return "[+ACTION][%s]\n%s\n%sPROFIT: [%s] %20.10f\t~ [%s] %20.10f\t~ [%%] %20.10f\n[-ACTION]" % (
				self.type_str(),
				"\n".join(actions_str),
				self.prefix,
				self.profit_currency,
				self.profit,
				self.conf["base_currency"],
				self.normalized_profit(),
				self.percent_profit()
			)

	def type_str(self):
		if self.type == TransactionType.Direct:
			return "DIRECT"
		elif self.type == TransactionType.Euro:
			return "EURO"
		elif self.type == TransactionType.Crypto:
			return "CRYPTO"
		elif self.type == TransactionType.Crazy:
			return "CRAZY"
		else:
			return "!!UNKNOWN!!"

	def addAction(self, action):
		self.actions.append(action)

	def check(self):
		self.actions_valid = []
		for action in self.actions:
			self.actions_valid.append(action.check(self.type))
		if all(self.actions_valid):
			self.valid = True
			return True
		self.valid = False
		return False

	def trace_funds(self):
		actions = filter(lambda a: not self.actions_valid[self.actions.index(a)], self.actions)
		for action in actions:
			if action.type == ActionType.Buy:
				print "[MISSING FUNDS REPORT] Cannot buy  [%s] %20.10f on [%s][%s] ~ [%s] balance: %20.10f; required: %20.10f %s" % (
					action.currency,
					action.amount,
					action.market.code(),
					action.product,
					action.for_c,
					action.market.balance(action.for_c),
					action.for_v + action.market.hold(action.for_c),
					action.for_c
				)
			if action.type == ActionType.Sell:
				print "[MISSING FUNDS REPORT] Cannot sell [%s] %20.10f on [%s][%s] ~ [%s] balance: %20.10f; required: %20.10f %s" % (
					action.currency,
					action.amount,
					action.market.code(),
					action.product,
					action.currency,
					action.market.balance(action.currency),
					action.amount + action.market.hold(action.currency),
					action.currency
				)

	def run(self):
		for action in self.actions:
			action.run()
			sleep(0.01)

	def finish(self):
		for action in self.actions:
			action.finish()

	def normalized_profit(self):
		if self.profit_currency == self.conf["base_currency"]:
			return self.profit
		else:
			return self.profit * self.mm.exchange_rate(self.profit_currency)

	def percent_profit(self):
		return (self.normalized_profit() / self.mm.exchange_rate(self.actions[0].currency)) * 100

	def check_threshold(self, threshold):
		if type(threshold) in (str, unicode):
			if threshold.endswith("%"):
				return self.percent_profit() >= float(threshold[:-1])
			elif threshold.endswith("*"):
				return self.normalized_profit() >= float(threshold[:-1])
		return self.profit >= float(threshold)

	def format_profit_too_low(self, threshold):
		if threshold.endswith("%"):
			return "(%.4f%% < %s)" % (self.percent_profit(), threshold)
		elif threshold.endswith("*"):
			return "(%.4f* < %s)" % (self.normalized_profit(), threshold)
		else:
			return "(%.4f < %s)" % (self.profit, threshold)

	def format_profit(self, threshold):
		if threshold.endswith("%"):
			return "(%.4f%%)" % self.percent_profit()
		elif threshold.endswith("*"):
			return "(%.4f*)" % self.normalized_profit()
		else:
			return "(%.4f)" % self.profit

	def format_no_funds(self):
		return "[!ACTION][%s][%s][%s] %20.10f %s ~ %.4f* ~ %.4f%% %s\tNO FUNDS OR STOPPED BY HOLD LEVEL" % (
			self.type_str(),
			self.actions[0].product,
			"%s_%s" % (self.actions[0].market.code(), self.actions[-1].market.code()),
			self.profit,
			self.profit_currency,
			self.normalized_profit(),
			self.percent_profit(),
			self.actions[0].currency
		)

	def currencies_str(self):
		currencies = []
		for a in self.actions:
			currencies.append(a.currency)
			currencies.append(a.for_c)
		return ", ".join(list(set(currencies)))


def calculate_actions(trades, settings, mm):

	def is_deal(value, eur=None):
		""" Check if deal is profitable """
		if not eur:
			return value > 0
		else:
			if eur == "buy":
				return value > settings["threshold"]["euro"]["sell"]
			if eur == "sell":
				return value < settings["threshold"]["euro"]["buy"]

	transactions = []
	skipped = []

	for dt in trades["direct"]:
		if is_deal(dt.profit()):
			currency = dt.product.split("_")
			if "direct" in settings["action_blacklist"]:
				print "[!ACTION][DIRECT][%s][%s] %20.10f BLACKLISTED\n" % (
					currency[0],
					"%s_%s" % (dt.m1.code(), dt.m2.code()),
					dt.profit()
				)
				continue

			x = settings["portions"][currency[0]]
			xcomm = dt.m1.commission_buy(x)

			t = Transaction(TransactionType.Direct, dt.product, dt.profit(), currency[1], settings, mm)
			t.addAction(
				Action(
					dt.m1,
					ActionType.Buy,
					dt.product,
					currency[0],
					xcomm,
					currency[1],
					xcomm * dt.m1.ask(dt.product)
				)
			)
			t.addAction(
				Action(
					dt.m2,
					ActionType.Sell,
					dt.product,
					currency[0],
					x,
					currency[1],
					x * dt.m2.bid(dt.product)
				)
			)

			try:
			    conf_threshold = settings["threshold"]["custom"]["direct"][dt.product]
			except KeyError:
			    try:
			    	conf_threshold = settings["threshold"]["custom"]["direct"][currency[0]]
			    except KeyError:
			    	conf_threshold = settings['threshold']['default']['direct']

			if t.check_threshold(conf_threshold):
				if t.check():
					transactions.append(t)
				else:
					# print "[!ACTION][DIRECT][%s][%s] %20.10f NO FUNDS" % (
					# 	currency[0],
					# 	"%s_%s" % (dt.m1.code(), dt.m2.code()),
					# 	dt.profit(),
					# )
					print t.format_no_funds()
					t.trace_funds()
					print "\n"
					t.skipped_by = "funds"
					skipped.append(t)
			else:
				t.skipped_by = "profit"
				skipped.append(t)

	for et in trades["euro"]:
		if is_deal(et.profit()["buy"], eur="buy"):
			currency = et.product1.split("_")
			if "euro" in settings["action_blacklist"]:
				print "[!ACTION][EURO][%s][%s] %20.10f BLACKLISTED\n" % (
					currency[0],
					"%s_%s" % (et.m1.code(), et.m2.code()),
					et.profit()["buy"]
				)
				continue

			x = settings["portions"][currency[0]]
			xcomm = et.m2.commission_buy(x)

			t = Transaction(TransactionType.Euro, et.product, et.profit()["buy"], "", settings, mm, additional_info="buy")
			t.addAction(
				Action(
					et.m2,
					ActionType.Buy,
					et.product2,
					currency[0],
					xcomm,
					et.product2.split("_")[1],
					xcomm * et.m2.ask(et.product2)
				)
			)
			t.addAction(
				Action(
					et.m1,
					ActionType.Sell,
					et.product1,
					currency[0],
					x,
					currency[1],
					x * et.m1.bid(et.product1)
				)
			)
			if t.check():
				transactions.append(t)
			else:
				# print "[!ACTION][EURO][%s][%s] %20.10f NO FUNDS" % (
				# 	currency[0],
				# 	"%s_%s" % (et.m1.code(), et.m2.code()),
				# 	et.profit()["buy"]
				# )
				print t.format_no_funds()
				t.trace_funds()
				print "\n"
				t.skipped_by = "funds"
				skipped.append(t)

		if is_deal(et.profit()["sell"], eur="sell"):
			currency = et.product1.split("_")
			if "euro" in settings["action_blacklist"]:
				print "[!ACTION][EURO][%s][%s] %20.10f BLACKLISTED\n" % (
					currency[0],
					"%s_%s" % (et.m1.code(), et.m2.code()),
					et.profit()["sell"]
				)
				continue

			x = settings["portions"][currency[0]]
			xcomm = et.m1.commission_buy(x)

			t = Transaction(TransactionType.Euro, et.product, et.profit()["sell"], "", settings, mm, additional_info="sell")
			t.addAction(
				Action(
					et.m2,
					ActionType.Sell,
					et.product2,
					currency[0],
					x,
					et.product2.split("_")[1],
					x * et.m2.bid(et.product2)
				)
			)
			t.addAction(
				Action(
					et.m1,
					ActionType.Buy,
					et.product1,
					currency[0],
					xcomm,
					currency[1],
					xcomm * et.m1.ask(et.product1)
				)
			)
			if t.check():
				transactions.append(t)
			else:
				# print "[!ACTION][EURO][%s][%s] %20.10f NO FUNDS" % (
				# 	currency[0],
				# 	"%s_%s" % (et.m1.code(), et.m2.code()),
				# 	et.profit()["sell"]
				# )
				print t.format_no_funds()
				t.trace_funds()
				print "\n"
				t.skipped_by = "funds"
				skipped.append(t)

	for ct in trades["crypto"]:
		if is_deal(ct.profit()["buy"]):
			if "crypto" in settings["action_blacklist"]:
				print "[!ACTION][CRYPTO][%s][%s] %20.10f BLACKLISTED\n" % (
					ct.product1,
					"%s_%s" % (ct.m1.code(), ct.m2.code()),
					ct.profit()["buy"]
				)
				continue
			cs = ct.product1.split("_")
			x = settings["portions"][cs[0]]

			a1_buy = ct.m1.commission_buy(x)
			a1_buy_for = a1_buy * ct.m1.ask(ct.product1)
			a2_sell = x
			a2_sell_for = ct.m2.commission_sell(a2_sell * ct.m2.bid(ct.product2))
			a3_buy = ct.m2.commission_buy(a1_buy_for)
			a3_buy_for = a3_buy * ct.m2.ask(ct.product3)

			t = Transaction(TransactionType.Crypto, ct.product1, ct.profit()["buy"], ct.product3.split("_")[1], settings, mm, additional_info="buy")
			t.addAction(
				Action(
					ct.m1,
					ActionType.Buy,
					ct.product1,
					cs[0],
					a1_buy,
					cs[1],
					a1_buy_for
				)
			)
			t.addAction(
				Action(
					ct.m2,
					ActionType.Sell,
					ct.product2,
					cs[0],
					a2_sell,
					ct.product2.split("_")[1],
					a2_sell_for
				)
			)
			t.addAction(
				Action(
					ct.m2,
					ActionType.Buy,
					ct.product3,
					cs[1],
					a3_buy,
					ct.product3.split("_")[1],
					a3_buy_for
				)
			)

			try:
			    conf_threshold = settings["threshold"]["custom"]["crypto"][ct.product1]
			except KeyError:
			    try:
			    	conf_threshold = settings["threshold"]["custom"]["crypto"][cs[0]]
			    except KeyError:
			    	conf_threshold = settings['threshold']['default']['crypto']

			if t.check_threshold(conf_threshold):
				if t.check():
					transactions.append(t)
				else:
					# print "[!ACTION][CRYPTO][%s][%s] %20.10f NO FUNDS" % (
					# 	ct.product1,
					# 	"%s_%s" % (ct.m1.code(), ct.m2.code()),
					# 	ct.profit()["buy"]
					# )
					print t.format_no_funds()
					t.trace_funds()
					print "\n"
					t.skipped_by = "funds"
					skipped.append(t)
			else:
				t.skipped_by = "profit"
				skipped.append(t)

		if is_deal(ct.profit()["sell"]):
			if "crypto" in settings["action_blacklist"]:
				print "[!ACTION][CRYPTO][%s][%s] %20.10f BLACKLISTED\n" % (
					ct.product1,
					"%s_%s" % (ct.m1.code(), ct.m2.code()),
					ct.profit()["sell"]
				)
				continue
			cs = ct.product1.split("_")
			x = settings["portions"][cs[0]]

			a1_sell = x
			a1_sell_for = ct.m1.commission_sell(a1_sell * ct.m1.bid(ct.product1))
			a2_sell = a1_sell_for
			a2_sell_for = ct.m2.commission_sell(a2_sell * ct.m2.bid(ct.product3))
			a3_buy = ct.m2.commission_buy(a1_sell)
			a3_buy_for = a3_buy * ct.m2.ask(ct.product2)

			t = Transaction(TransactionType.Crypto, ct.product1, ct.profit()["sell"], ct.product3.split("_")[1], settings, mm, additional_info="sell")
			t.addAction(
				Action(
					ct.m1,
					ActionType.Sell,
					ct.product1,
					cs[0],
					a1_sell,
					cs[1],
					a1_sell_for
				)
			)
			t.addAction(
				Action(
					ct.m2,
					ActionType.Sell,
					ct.product3,
					cs[1],
					a2_sell,
					ct.product3.split("_")[1],
					a2_sell_for
				)
			)
			t.addAction(
				Action(
					ct.m2,
					ActionType.Buy,
					ct.product2,
					cs[0],
					a3_buy,
					ct.product2.split("_")[1],
					a3_buy_for
				)
			)

			try:
			    conf_threshold = settings["threshold"]["custom"]["crypto"][ct.product1]
			except KeyError:
			    try:
			    	conf_threshold = settings["threshold"]["custom"]["crypto"][cs[0]]
			    except KeyError:
			    	conf_threshold = settings['threshold']['default']['crypto']

			if t.check_threshold(conf_threshold):
				if t.check():
					transactions.append(t)
				else:
					# print "[!ACTION][CRYPTO][%s][%s] %20.10f NO FUNDS" % (
					# 	ct.product1,
					# 	"%s_%s" % (ct.m1.code(), ct.m2.code()),
					# 	ct.profit()["sell"]
					# )
					print t.format_no_funds()
					t.trace_funds()
					print "\n"
					t.skipped_by = "funds"
					skipped.append(t)
			else:
				t.skipped_by = "profit"
				skipped.append(t)

	for cct in trades["crazy"]:
		if is_deal(cct.profit()["buy"]):
			if "crazy" in settings["action_blacklist"]:
				print "[!ACTION][CRAZY ][%s][%s] %20.10f BLACKLISTED\n" % (
					cct.product1,
					"%s_%s" % (cct.m1.code(), cct.m2.code()),
					cct.profit()["buy"]
				)
				continue
			cs = cct.product1.split("_")
			x = settings["portions"][cs[0]]

			a1_buy = cct.m1.commission_buy(x)
			a1_buy_for = a1_buy * cct.m1.ask(cct.product1)
			a2_sell = x
			a2_sell_for = ct.m2.commission_sell(a2_sell * cct.m2.bid(cct.product2))
			a3_buy = cct.m3.commission_buy(a1_buy_for)
			a3_buy_for = a3_buy * cct.m3.ask(cct.product3)

			t = Transaction(TransactionType.Crazy, cct.product1, cct.profit()["buy"], cct.product3.split("_")[1], settings, mm, additional_info="buy")
			t.addAction(
				Action(
					cct.m1,
					ActionType.Buy,
					cct.product1,
					cs[0],
					a1_buy,
					cs[1],
					a1_buy_for
				)
			)
			t.addAction(
				Action(
					cct.m2,
					ActionType.Sell,
					cct.product2,
					cs[0],
					a2_sell,
					cct.product2.split("_")[1],
					a2_sell_for
				)
			)
			t.addAction(
				Action(
					cct.m3,
					ActionType.Buy,
					cct.product3,
					cs[1],
					a3_buy,
					cct.product3.split("_")[1],
					a3_buy_for
				)
			)

			try:
			    conf_threshold = settings["threshold"]["custom"]["crazy"][cct.product1]
			except KeyError:
			    try:
			    	conf_threshold = settings["threshold"]["custom"]["crazy"][cs[0]]
			    except KeyError:
			    	conf_threshold = settings['threshold']['default']['crazy']

			if t.check_threshold(conf_threshold):
				if t.check():
					transactions.append(t)
				else:
					# print "[!ACTION][CRYPTO][%s][%s] %20.10f NO FUNDS" % (
					# 	ct.product1,
					# 	"%s_%s" % (ct.m1.code(), ct.m2.code()),
					# 	ct.profit()["buy"]
					# )
					print t.format_no_funds()
					t.trace_funds()
					print "\n"
					t.skipped_by = "funds"
					skipped.append(t)
			else:
				t.skipped_by = "profit"
				skipped.append(t)

		if is_deal(cct.profit()["sell"]):
			if "crazy" in settings["action_blacklist"]:
				print "[!ACTION][CRAZY ][%s][%s] %20.10f BLACKLISTED\n" % (
					cct.product1,
					"%s_%s" % (cct.m1.code(), cct.m2.code()),
					cct.profit()["sell"]
				)
				continue
			cs = cct.product1.split("_")
			x = settings["portions"][cs[0]]

			a1_sell = x
			a1_sell_for = cct.m1.commission_sell(a1_sell * cct.m1.bid(cct.product1))
			a2_sell = a1_sell_for
			a2_sell_for = cct.m3.commission_sell(a2_sell * cct.m3.bid(cct.product3))
			a3_buy = cct.m2.commission_buy(a1_sell)
			a3_buy_for = a3_buy * cct.m2.ask(cct.product2)

			t = Transaction(TransactionType.Crazy, cct.product1, cct.profit()["sell"], cct.product3.split("_")[1], settings, mm, additional_info="sell")
			t.addAction(
				Action(
					cct.m1,
					ActionType.Sell,
					cct.product1,
					cs[0],
					a1_sell,
					cs[1],
					a1_sell_for
				)
			)
			t.addAction(
				Action(
					cct.m3,
					ActionType.Sell,
					cct.product3,
					cs[1],
					a2_sell,
					cct.product3.split("_")[1],
					a2_sell_for
				)
			)
			t.addAction(
				Action(
					cct.m2,
					ActionType.Buy,
					cct.product2,
					cs[0],
					a3_buy,
					cct.product2.split("_")[1],
					a3_buy_for
				)
			)

			try:
			    conf_threshold = settings["threshold"]["custom"]["crazy"][cct.product1]
			except KeyError:
			    try:
			    	conf_threshold = settings["threshold"]["custom"]["crazy"][cs[0]]
			    except KeyError:
			    	conf_threshold = settings['threshold']['default']['crazy']

			if t.check_threshold(conf_threshold):
				if t.check():
					transactions.append(t)
				else:
					# print "[!ACTION][CRYPTO][%s][%s] %20.10f NO FUNDS" % (
					# 	ct.product1,
					# 	"%s_%s" % (ct.m1.code(), ct.m2.code()),
					# 	ct.profit()["sell"]
					# )
					print t.format_no_funds()
					t.trace_funds()
					print "\n"
					t.skipped_by = "funds"
					skipped.append(t)
			else:
				t.skipped_by = "profit"
				skipped.append(t)

	return transactions, skipped


def pick_transaction(transactions, settings):
	direct = filter(lambda t: t.type == TransactionType.Direct, transactions)
	euro = filter(lambda t: t.type == TransactionType.Euro, transactions)
	crypto = filter(lambda t: t.type == TransactionType.Crypto, transactions)
	crazy = filter(lambda t: t.type == TransactionType.Crazy, transactions)

	def sort_transactions(transactions):
		return sorted(transactions, reverse=True, key=lambda t: t.normalized_profit())

	def pick_euro(transactions):
		buy = map(
			lambda t: {"profit": t.profit - settings["threshold"]["euro"]["sell"], "t": t},
			filter(
				lambda t: t.additional_info == "buy",
				euro
			)
		)
		sell = map(
			lambda t: {"profit": settings["threshold"]["euro"]["buy"] - t.profit, "t": t},
			filter(
				lambda t: t.additional_info == "sell",
				euro
			)
		)

		buy_t = None
		sell_t = None
		if buy:
			buy_t = sorted(buy, reverse=True, key=lambda t: t["profit"])[0]
		if sell:
			sell_t = sorted(sell, reverse=True, key=lambda t: t["profit"])[0]

		if buy_t and sell_t:
			if buy_t["profit"] >= sell_t["profit"]:
				return buy_t["t"]
			else:
				return sell_t["t"]
		else:
			if buy_t:
				return buy_t["t"]
			else:
				return sell_t["t"]


	# #1: direct-trade
	if direct:
		return sort_transactions(direct)[0]

	# #2: crypto-trade
	if crypto:
		return sort_transactions(crypto)[0]

	# #3: crazy-crypto-trade
	if crazy:
		return sort_transactions(crazy)[0]

	# #4: euro-trade
	if euro:
		return pick_euro(euro)

	return None
