import re
import math
import json
import requests

from keycache import KeyCache


def round_up(value, precision):
	m = 10 ** precision
	return math.ceil(value * m) / m


def round_down(value, precision):
	m = 10 ** precision
	return math.floor(value * m) / m


def generic_get_orderbook(conf, product):
	r = requests.get(
		conf["api"]["public"]["orderbook"].format(
			product=conf["products"][product]
		),
		headers={
			"User-Agent": "Mozilla/5.0 (Windows NT 5.1; rv:5.0.1) Gecko/20100101 Firefox/5.0.1"
		}
	)
	data = r.text
	while r.status_code != 200:
		raise Exception("HTTP Error: Service returned %d." % r.status_code)

	return data


def generic_parse_orderbook(data):
	return json.loads(data)


def rtm_supported_cryptocurrencies(conf):
	rtm_cryptos = [] 
	for crypto, crypto_sum in conf['sums'].items():
		rtm_cryptos.append(crypto.lower())

	return rtm_cryptos


def load_key(key_desc):
	if type(key_desc) in (str, unicode) and key_desc.startswith("@"):
		m = re.match(r'^@(.*)\[(.*)\.(.*)\]$', key_desc)
		if not m:
			print "[RTM-API] Key description %s does not match @file[realm.name] pattern." % key_desc
			raise Exception("Key description %s does not match @file[realm.name] pattern." % key_desc)
		key_file = m.group(1)
		key_realm = m.group(2)
		key_name = m.group(3)

		key_data = KeyCache.get_keyfile(key_file)
		return key_data[key_realm][key_name]
	else:
		return key_desc
