import requests
import json


def exchange_rates_0(conf):
	r = requests.get(conf["exchange_rates"]["url"].format(base_currency=conf["base_currency"]))
	while r.status_code != 200:
		raise Exception("HTTP Error: Service returned %d." % r.status_code)

	data = json.loads(r.text)["rates"]
	return dict([(k, 1 / v) for k, v in data.iteritems()])


def exchange_rates(conf):
	response = requests.get('https://api.exchangerate-api.com/v4/latest/{0}'.format(conf['base_currency']), timeout=5).json()
	if response["rates"]:
		data = response["rates"]
	else:
		raise Exception('Failed to download Exchange Rates. Why?\n{0}'.format(response))
	return dict([(k, 1 / v) for k, v in data.iteritems()])

