import requests
import json


def exchange_rates(conf):
	r = requests.get(conf["exchange_rates"]["url"])
	while r.status_code != 200:
		raise Exception("HTTP Error: Service returned %d." % r.status_code)

	data = json.loads(r.text)[0]["rates"]
	return dict([(rate["code"], rate["mid"]) for rate in data])
