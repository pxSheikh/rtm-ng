import requests
import hmac
import hashlib
import base64
import json
from time import time

import api_utils


def orderbook(conf, product):
	data = api_utils.generic_get_orderbook(conf, product)
	return json.loads(data)["result"][conf["products"][product]]


def buy(conf, product, amount, for_v):
	return _api_call(conf, "buy", product, amount)


def sell(conf, product, amount, for_v):
	return _api_call(conf, "sell", product, amount)


def balances(conf):
	nonce = int(time() * 100)
	req = requests.Request(
		"POST",
		conf["api"]["private"]["balances-url"],
		headers={
			"API-Key": api_utils.load_key(conf["api"]["private"]["api-key-public"]),
		},
		data={
			"nonce": nonce,
		}
	).prepare()

	# TODO: url
	hash_string = "%s%s" % ("/0/private/Balance", hashlib.sha256("%d%s" % (nonce, req.body)).digest())
	req.headers["API-Sign"] = base64.b64encode(hmac.new(
		str(base64.b64decode(api_utils.load_key(conf["api"]["private"]["api-key-private"]))),
		hash_string,
		digestmod=hashlib.sha512
	).digest())

	with requests.Session() as session:
		r = session.send(req)

	res = json.loads(r.text)

	output = {}
	for k, v in conf["currencies"].iteritems():
		if v in res["result"]:
			output[k] = res["result"][v]

	return output


def _api_call(conf, type, product, amount):
	if type == "buy":
		sanitize = api_utils.round_up
	elif type == "sell":
		sanitize = api_utils.round_down

	precision = 8

	nonce = int(time() * 100)
	req = requests.Request(
		"POST",
		conf["api"]["private"]["action-url"],
		headers={
			"API-Key": api_utils.load_key(conf["api"]["private"]["api-key-public"]),
		},
		data={
			"nonce": nonce,
			"pair": conf["products"][product],
			"type": type,
			"ordertype": "market",
			"volume": sanitize(amount, precision)
		}
	).prepare()

	# TODO: url
	hash_string = "%s%s" % ("/0/private/AddOrder", hashlib.sha256("%d%s" % (nonce, req.body)).digest())
	req.headers["API-Sign"] = base64.b64encode(hmac.new(
		str(base64.b64decode(api_utils.load_key(conf["api"]["private"]["api-key-private"]))),
		hash_string,
		digestmod=hashlib.sha512
	).digest())

	print req.body
	if conf["test_mode"]:
		return req.headers, req.body, {"test_mode": "TEST MODE"}, "TEST MODE"

	with requests.Session() as session:
		r = session.send(req)

	print r.text
	return req.headers, req.body, r.headers, r.text
