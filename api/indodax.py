import requests
import hmac
import hashlib
import json
from time import time, sleep

import api_utils


def orderbook(conf, product):
	data = api_utils.generic_get_orderbook(conf, product)
	parsed_ob = api_utils.generic_parse_orderbook(data)

	ob = {}
	ob['asks'] = [[(pair[0]), (pair[1])] for pair in parsed_ob['sell']]
	ob['bids'] = [[(pair[0]), (pair[1])] for pair in parsed_ob['buy']]
	return ob


def buy(conf, product, amount, for_v):
	return _api_call(conf, "buy", product, amount, for_v)


def sell(conf, product, amount, for_v):
	return _api_call(conf, "sell", product, amount, for_v)


def balances(conf):
	nonce = str(int(time() * 2358))

	req = requests.Request(
		"POST",
		conf["api"]["private"]["url"],
		headers={
			"Key": api_utils.load_key(conf["api"]["private"]["api-key-public"]),
		},
		data={
			"method": 'getInfo',
			"nonce": nonce
		}
	).prepare()
	req.headers["Sign"] = hmac.new(
		str(api_utils.load_key(conf["api"]["private"]["api-key-private"])).encode('utf-8'),
		str(req.body).encode('utf-8'),
		digestmod=hashlib.sha512
	).hexdigest()

	with requests.Session() as session:
		r = session.send(req)

	res = json.loads(r.text)

	output = {}
	for currency, amount in res['return']['balance'].iteritems():
		if str(currency) in conf["currencies"].values():
			output[conf["currencies"].keys()[conf["currencies"].values().index(currency)]] = amount
	return output


def _api_call(conf, type, product, amount, for_v):
	p = product.split("_")
	amount_precision = 8
	price_precision = 8

	if type == "buy":
		sanitize = api_utils.round_up
		price_string = '{:0.0{}f}'.format(float(sanitize(for_v / amount, price_precision)), price_precision)
		
		p_param = p[1]
		if p_param == "INR":
			amount_precision = 0

		p_param_amount = '{:0.0{}f}'.format(float(sanitize((float(price_string) * amount), amount_precision)), amount_precision)
	elif type == "sell":
		sanitize = api_utils.round_down
		price_string = '{:0.0{}f}'.format(float(sanitize(for_v / amount, price_precision)), price_precision)
		p_param = p[0]
		p_param_amount = '{:0.0{}f}'.format(float(sanitize(amount, amount_precision)), amount_precision)


	action_fail_retry = [1, 2, 3]
	for action in action_fail_retry:
		nonce = str(int(time() * 2358))

		req = requests.Request(
			"POST",
			conf["api"]["private"]["url"],
			headers={
				"Key": api_utils.load_key(conf["api"]["private"]["api-key-public"]),
			},
			data={
				"method": "trade",
				"nonce": nonce,
				"pair": conf["products"][product],
				"type": type,
				"price": price_string,
				"{0}".format(p_param.lower()): p_param_amount
			}
		).prepare()
		req.headers["Sign"] = hmac.new(
			str(api_utils.load_key(conf["api"]["private"]["api-key-private"])).encode('utf-8'),
			str(req.body).encode('utf-8'),
			digestmod=hashlib.sha512
		).hexdigest()

		print req.body
		if conf["test_mode"]:
			return req.headers, req.body, {"test_mode": "TEST MODE"}, "TEST MODE"

		with requests.Session() as session:
			r = session.send(req)

		answear = json.loads(r.text)
		
		if answear['success'] == 1:
			print answear
			return req.headers, req.body, r.headers, r.text

		elif answear['success'] == 0 and answear['error_code'] == 'invalid_nonce': 
			print('Nonce {0} fail [!]\n Sleeping for 2 sec...'.format(nonce))
			sleep(2) 
			continue
		else: 
			print answear
			return req.headers, req.body, r.headers, r.text
