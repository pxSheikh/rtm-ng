import requests
import json
from random import choice

import api_utils


def exchange_rates(conf):
	api_key = choice(api_utils.load_key(conf["crypto_rates"]["key-pool"]))
	print "[COINMARKETCAP][API] Using key %s(...)%s." % (api_key[:6], api_key[-6:])
	with requests.Session() as session:
		session.headers.update({
			"X-CMC_PRO_API_KEY": api_key
		})

		r = session.get(
			conf["crypto_rates"]["url"],
			params={
				"start": 1,
				"limit": conf["crypto_rates"]["limit"],
				"convert": conf["base_currency"]
			}
		)

	while r.status_code != 200:
		raise Exception("HTTP Error: Service returned %d." % r.status_code)

	data = json.loads(r.text)["data"]
	x = dict([(rate["symbol"], rate["quote"][conf["base_currency"]]["price"]) for rate in data])

	return x