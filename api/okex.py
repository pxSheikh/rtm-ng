import requests
import hmac
import hashlib
import base64
import json
from time import time

import api_utils


def orderbook(conf, product):
	data = api_utils.generic_get_orderbook(conf, product)
	return api_utils.generic_parse_orderbook(data)


def buy(conf, product, amount, for_v):
	return _api_call(conf, "buy", product, amount, for_v)


def sell(conf, product, amount, for_v):
	return _api_call(conf, "sell", product, amount, for_v)


def balances(conf):
	timestamp = time()
	req = requests.Request(
		"GET",
		conf["api"]["private"]["balances-url"],
		headers={
			"Content-Type": "application/json",
			"OK-ACCESS-KEY": api_utils.load_key(conf["api"]["private"]["api-key-public"]),
			"OK-ACCESS-TIMESTAMP": str(timestamp),
			"OK-ACCESS-PASSPHRASE": api_utils.load_key(conf["api"]["private"]["api-key-passphrase"])
		}
	).prepare()
	message = '{0}{1}{2}'.format(str(timestamp), 'GET', '/api/spot/v3/accounts')

	req.headers["OK-ACCESS-SIGN"] = base64.b64encode(hmac.new(
		bytes(api_utils.load_key(conf["api"]["private"]["api-key-private"]).encode('utf-8')),
		bytes(message).encode('utf-8'),
		digestmod=hashlib.sha256
	).digest())

	with requests.Session() as session:
		r = session.send(req)

	res = json.loads(r.text)

	output = {}
	if r.status_code == 200 and res:
		for balance in res:
			if str(balance['currency']) in conf["currencies"].values():
				output[conf["currencies"].keys()[conf["currencies"].values().index(balance['currency'])]] = balance['available']
	elif r.status_code == 200:
		print '[OKEX][API] There are no assets in balances.'
	else:
		print '[OKEX][API] Failed to download balances.'
	return output


def _api_call(conf, type, product, amount, for_v):
	if type == "buy":
		sanitize = api_utils.round_up
	elif type == "sell":
		sanitize = api_utils.round_down

	price_precision = 8
	amount_precision = 8

	if product in ("BTC_USDT"):
		price_precision = 1
		amount_precision = 8
	elif product in ("XMR_USDT", "ZEC_USDT", "DASH_USDT"):
		price_precision = 1
		amount_precision = 6
	elif product in ("LSK_USDT"):
		price_precision = 2
		amount_precision = 8
	elif product in ("LTC_USDT", "ETH_USDT"):
		price_precision = 2
		amount_precision = 6
	elif product in ("BTG_USDT", "QTUM_USDT", "NEO_USDT"):
		price_precision = 2
		amount_precision = 5
	elif product in ("BCH_USDT", "BSV_USDT"):
		price_precision = 2
		amount_precision = 4
	elif product in ("XMR_ETH", "ZEC_ETH", "DASH_ETH"):
		price_precision = 3
		amount_precision = 6
	elif product in ("ETC_USDT"):
		price_precision = 3
		amount_precision = 5
	elif product in ("EOS_USDT", "NANO_USDT"):
		price_precision = 3
		amount_precision = 4
	elif product in ("XMR_BTC", "DASH_BTC", "LTC_ETH", "ZEC_BTC"):
		price_precision = 4
		amount_precision = 6
	elif product in ("QTUM_ETH", "NEO_ETH"):
		price_precision = 4
		amount_precision = 5
	elif product in ("USDC_USDT"):
		price_precision = 4
		amount_precision = 4
	elif product in ("XLM_USDT", "XRP_USDT", "XEM_USDT"):
		price_precision = 4
		amount_precision = 3
	elif product in ("LSK_ETH"):
		price_precision = 5
		amount_precision = 8
	elif product in ("ETH_BTC"):
		price_precision = 5
		amount_precision = 6
	elif product in ("BTG_BTC", "ETC_ETH"):
		price_precision = 5
		amount_precision = 5
	elif product in ("BCH_BTC", "EOS_ETH", "NANO_ETH"):
		price_precision = 5
		amount_precision = 4
	elif product in ("TRX_USDT"):
		price_precision = 5
		amount_precision = 2
	elif product in ("LSK_BTC"):
		price_precision = 6
		amount_precision = 8
	elif product in ("LTC_BTC"):
		price_precision = 6
		amount_precision = 6
	elif product in ("QTUM_BTC", "NEO_BTC"):
		price_precision = 6
		amount_precision = 5
	elif product in ("BSV_BTC", "BTT_USDT", "NANO_BTC", "DOGE_USDT"):
		price_precision = 6
		amount_precision = 4
	elif product in ("XLM_ETH", "XRP_ETH", "XEM_ETH"):
		price_precision = 6
		amount_precision = 3
	elif product in ("ETC_BTC"):
		price_precision = 7
		amount_precision = 5
	elif product in ("EOS_BTC"):
		price_precision = 7
		amount_precision = 4
	elif product in ("XEM_BTC"):
		price_precision = 7
		amount_precision = 3
	elif product in ("TRX_ETH"):
		price_precision = 7
		amount_precision = 2
	elif product in ("BTT_ETH"):
		price_precision = 8
		amount_precision = 4
	elif product in ("XLM_BTC", "XRP_BTC"):
		price_precision = 8
		amount_precision = 3
	elif product in ("TRX_BTC"):
		price_precision = 8
		amount_precision = 2
	elif product in ("BTT_BTC"):
		price_precision = 9
		amount_precision = 4

	timestamp = time()
	data = {
		"type": 'limit',
		"side": type,
		"instrument_id": str(conf["products"][product]),
		"margin_trading": '1',
		"order_type": '0',
		"price": '{:0.0{}f}'.format(float(sanitize(for_v / amount, price_precision)), price_precision),
		"size": '{:0.0{}f}'.format(float(sanitize(amount, amount_precision)), amount_precision)
	}
	message = '{0}{1}{2}{3}'.format(str(timestamp), 'POST', '/api/spot/v3/orders', json.dumps(data))
	req = requests.Request(
		"POST",
		conf["api"]["private"]["action-url"],
		headers={
			"Content-Type": "application/json",
			"OK-ACCESS-KEY": api_utils.load_key(conf["api"]["private"]["api-key-public"]),
			"OK-ACCESS-TIMESTAMP": str(timestamp),
			"OK-ACCESS-PASSPHRASE": api_utils.load_key(conf["api"]["private"]["api-key-passphrase"])
		},
		data=json.dumps(data)
	).prepare()
	req.headers["OK-ACCESS-SIGN"] = base64.b64encode(hmac.new(
		bytes(api_utils.load_key(conf["api"]["private"]["api-key-private"]).encode('utf-8')),
		bytes(message).encode('utf-8'),
		digestmod=hashlib.sha256
	).digest())

	print req.body
	if conf["test_mode"]:
		return req.headers, req.body, {"test_mode": "TEST MODE"}, "TEST MODE"

	with requests.Session() as session:
		r = session.send(req)

	print r.text
	return req.headers, req.body, r.headers, r.text
