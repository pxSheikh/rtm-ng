import requests
import hmac
import hashlib
import json
from time import time

import api_utils


def orderbook(conf, product):
	data = api_utils.generic_get_orderbook(conf, product)
	return api_utils.generic_parse_orderbook(data)


def buy(conf, product, amount, for_v):
	return _api_call(conf, "buy", product, amount, for_v)


def sell(conf, product, amount, for_v):
	return _api_call(conf, "sell", product, amount, for_v)


def balances(conf):
	message = 'recvWindow={0}&timestamp={1}'.format('21000', str(int(time() * 1000)))
	signature = hmac.new(
		str(api_utils.load_key(conf["api"]["private"]["api-key-private"])),
		message,
		digestmod=hashlib.sha256
	).hexdigest()

	query_string = '{0}?{1}&signature={2}'.format(conf["api"]["private"]["balances-url"], message, signature)
	req = requests.Request(
		"GET",
		query_string,
		headers={
			"X-MBX-APIKEY": str(api_utils.load_key(conf["api"]["private"]["api-key-public"])),
		}
	).prepare()

	with requests.Session() as session:
		r = session.send(req)

	print "[%s][API] Currently used weight: %s." % (conf["code"], r.headers["X-MBX-USED-WEIGHT"])

	res = json.loads(r.text)
	output = {}
	for balance in res["balances"]:
		if str(balance["asset"]) in conf["currencies"].values():
			output[conf["currencies"].keys()[conf["currencies"].values().index(balance["asset"])]] = balance["free"]

	return output


def _api_call(conf, type, product, amount, for_v):
	if type == "buy":
		sanitize = api_utils.round_up
	elif type == "sell":
		sanitize = api_utils.round_down

	price_precision = 8
	amount_precision = 8

	if product in ('BTT_BTC'):
		price_precision = 8
		amount_precision = 3
	elif product in ('XLM_BTC', 'XRP_BTC', 'TRX_BTC', 'BAT_BTC', 'XLM_ETH', 'XRP_ETH', 'TRX_ETH', 'BAT_ETH', 'XVG_BTC', 'XVG_ETH', 'XEM_BTC', 'XEM_ETH', 'DOGE_BTC'):
		price_precision = 8
		amount_precision = 0
	elif product in ('BTT_USDT', 'BTT_USDC', 'BTT_PAX'):
		price_precision = 7
		amount_precision = 3
	elif product in ('LSK_BTC', 'EOS_BTC', 'NANO_BTC'):
		price_precision = 7
		amount_precision = 2
	elif product in ('DOGE_USDT', 'DOGE_USDC', 'DOGE_PAX'):
		price_precision = 7
		amount_precision = 0
	elif product in ('ETH_BTC', 'BCH_BTC', 'XMR_BTC', 'DASH_BTC', 'ZEC_BTC'):
		price_precision = 6
		amount_precision = 3
	elif product in ('LTC_BTC', 'BTG_BTC', 'BTG_ETH', 'LSK_ETH', 'EOS_ETH', 'ETC_BTC', 'ETC_ETH', 'NEO_BTC', 'NEO_ETH', 'QTUM_BTC', 'QTUM_ETH', 'NANO_ETH'):
		price_precision = 6
		amount_precision = 2
	elif product in ('LTC_ETH', 'XMR_ETH', 'DASH_ETH', 'ZEC_ETH'):
		price_precision = 5
		amount_precision = 3
	elif product in ('XLM_USDT', 'XRP_USDT', 'TRX_USDT', 'XLM_USC', 'XRP_USDC', 'TRX_USDC', 'TRX_XRP', 'BTT_TRX'):
		price_precision = 5
		amount_precision = 1
	elif product in ('USDC_USDT', 'BAT_USDT', 'EOS_USDT', 'BAT_USDC', 'EOS_USDC', 'ETC_USDT', 'NANO_USDT'):
		price_precision = 4
		amount_precision = 2
	elif product in ('ETC_USDC', 'ETC_PAX', 'NEO_USDT', 'NEO_USDC', 'NEO_PAX', 'QTUM_USDT'):
		price_precision = 3
		amount_precision = 3
	elif product in ('BTC_USDT', 'BTC_USDC'):
		price_precision = 2
		amount_precision = 6
	elif product in ('LTC_USDT', 'ETH_USDT', 'BCH_USDT', 'XMR_USDT', 'DASH_USDT', 'LTC_USDC', 'ETH_USDC', 'BCH_USDC', 'ZEC_USDT', 'ZEC_USDC', 'ZEC_PAX'):
		price_precision = 2
		amount_precision = 5

	order_message = 'symbol={0}&side={1}&type={2}&timeInForce={3}&quantity={4}&price={5}&recvWindow={6}&timestamp={7}'.format(
		conf["products"][product],
		type.upper(),
		'LIMIT',
		'GTC',
		'{:0.0{}f}'.format(float(sanitize(amount, amount_precision)), amount_precision),
		'{:0.0{}f}'.format(float(sanitize(for_v / amount, price_precision)), price_precision),
		'21000',
		str(int(time() * 1000))
	)
	signature = hmac.new(
		bytes(api_utils.load_key(conf["api"]["private"]["api-key-private"])),
		order_message,
		digestmod=hashlib.sha256
	).hexdigest()

	query_string = '{0}?{1}&signature={2}'.format(
		conf["api"]["private"]["action-url"],
		order_message,
		signature
	)
	req = requests.Request(
		"POST",
		query_string,
		headers={
			"X-MBX-APIKEY": str(api_utils.load_key(conf["api"]["private"]["api-key-public"])),
		}
	).prepare()

	if conf["test_mode"]:
		return req.headers, req.body, {"test_mode": "TEST MODE"}, "TEST MODE"

	with requests.Session() as session:
		r = session.send(req)

	print "[%s][API] Currently used weight: %s." % (conf["code"], r.headers["X-MBX-USED-WEIGHT"])

	print r.text
	return req.headers, req.body, r.headers, r.text
