import requests
import json

import api_utils


def exchange_rates(conf):
	_conf = conf

	rtm_cryptos = api_utils.rtm_supported_cryptocurrencies(conf)

	# * TRANSLATOR ~ Gecko -> Crypto 
	# * Gecko ids : Gecko symbols | eg. bitcoin : btc 
	gecko_to_crypto = {}
	gecko_ids = []
	coins_response = requests.get('{0}/coins/list'.format(conf['crypto_rates']['url']), timeout=15).json()

	# print 'coins_response: {0}'.format(coins_response)

	for item in coins_response:
		_symbol = item['symbol'].lower()
		_id = item['id'].lower()

		if _symbol == 'gxc' and _id == 'game-x-coin':
			coins_response.remove(item)
		elif _symbol in rtm_cryptos:
			gecko_to_crypto[item['id']] = item['symbol']
			gecko_ids.append(item['id'])
		elif _symbol == 'miota':
			gecko_to_crypto[item['id']] = 'IOTA'
			gecko_ids.append(item['id'])
		elif _symbol == 'yoyow':
			gecko_to_crypto[item['id']] = 'YOYO'
			gecko_ids.append(item['id'])

	# print 'Gecko ids: {0}'.format(gecko_ids)

	output = {}
	prices_response = requests.get('{0}/simple/price?ids={1}&vs_currencies={2}'.format(conf['crypto_rates']['url'], ','.join(gecko_ids), conf['base_currency']), timeout=5).json()
	for gecko_id, price_dict in prices_response.items():
		output[str(gecko_to_crypto[gecko_id].upper())] = float(price_dict[conf['base_currency'].lower()])

	missing_cryptos = [crypto for crypto in rtm_cryptos if crypto.upper() not in output]
	if not missing_cryptos:
		print ' + [ COINGECKO ] All prices found  + '
	else:
		print ' - [ COINGECKO ] Missing cryptos: {0}  - '.format(missing_cryptos)
		if 'eur' in missing_cryptos:
			print ' + Downloading fiat EUR price +'
			_fiat = fiat_rates(_conf)
			output['EUR'] = _fiat['EUR']
			missing_cryptos.remove('eur')

	print '*~* Coingecko Crypto Prices *~*'
	for symbol, rate in output.items():
		print '*** {0} : {1} {2} ***'.format(symbol, rate, conf['base_currency'])
	print '*~* Thank You Coingecko API *~*'
	print ' ~  RTM_Cryptos: {0} | Found prices: {1} | Missing: {2}  ~ '.format(len(rtm_cryptos), len(output), len(missing_cryptos))
	return output


def fiat_rates(conf):
	response = requests.get('https://api.exchangerate-api.com/v4/latest/{0}'.format(conf['base_currency']), timeout=5).json()
	if response["rates"]:
		data = response["rates"]
	else:
		raise Exception('--- Failed to download Fiat Rates in Coingecko API. \n--- Why: \n{0}'.format(response))
	return dict([(k, 1 / v) for k, v in data.iteritems()])

