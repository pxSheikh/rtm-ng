import requests
import hmac
import hashlib
import base64
import json
from time import time

import api_utils


def orderbook(conf, product):
	data = api_utils.generic_get_orderbook(conf, product)
	return api_utils.generic_parse_orderbook(data)


def buy(conf, product, amount, for_v):
	return _api_call(conf, "buy", product, amount, for_v)


def sell(conf, product, amount, for_v):
	return _api_call(conf, "sell", product, amount, for_v)


def balances(conf):
	timestamp = int(time())
	req = requests.Request(
		"GET",
		conf["api"]["private"]["balances-url"],
		headers={
			"CB-ACCESS-KEY": api_utils.load_key(conf["api"]["private"]["api-key-public"]),
			"CB-ACCESS-TIMESTAMP": str(timestamp),
			"CB-ACCESS-PASSPHRASE": api_utils.load_key(conf["api"]["private"]["api-key-passphrase"]),
			"User-Agent": "Mozilla/5.0 (Windows NT 5.1; rv:5.0.1) Gecko/20100101 Firefox/5.0.1",
			"Content-Type": "application/json"
		}
	).prepare()

	hash_string = "%d%s%s" % (
		timestamp,
		"GET",
		"/accounts"
	)
	req.headers["CB-ACCESS-SIGN"] = base64.b64encode(hmac.new(
		str(base64.b64decode(api_utils.load_key(conf["api"]["private"]["api-key-private"]))),
		hash_string,
		digestmod=hashlib.sha256
	).digest())

	with requests.Session() as session:
		r = session.send(req)

	res = json.loads(r.text)

	output = {}
	for account in res:
		if str(account["currency"]) in conf["currencies"].values():
			output[conf["currencies"].keys()[conf["currencies"].values().index(account["currency"])]] = account["available"]

	return output


def _api_call(conf, type, product, amount, for_v):
	if type == "buy":
		sanitize = api_utils.round_up
	elif type == "sell":
		sanitize = api_utils.round_down

	price_precision = 8
	amount_precision = 8

	if product in ('DASH_BTC'):
		price_precision = 8
		amount_precision = 3
	elif product in ('XRP_BTC', 'XLM_BTC', 'BAT_ETH'):
		price_precision = 8
		amount_precision = 0
	elif product in ('LTC_BTC', 'ETC_BTC'):
		price_precision = 6
		amount_precision = 8
	elif product in ('ZEC_BTC'):
		price_precision = 6
		amount_precision = 4
	elif product in ('EOS_BTC'):
		price_precision = 6
		amount_precision = 1
	elif product in ('XLM_EUR', 'BAT_USDC'):
		price_precision = 6
		amount_precision = 0
	elif product in ('ETH_BTC', 'BCH_BTC'):
		price_precision = 5
		amount_precision = 8
	elif product in ('XRP_EUR'):
		price_precision = 4
		amount_precision = 0
	elif product in ('ETC_EUR'):
		price_precision = 3
		amount_precision = 8
	elif product in ('EOS_EUR'):
		price_precision = 3
		amount_precision = 1
	elif product in ('BTC_EUR', 'LTC_EUR', 'ETH_EUR', 'BCH_EUR', 'BTC_USDC', 'ETH_USDC', 'ZEC_USDC'):
		price_precision = 2
		amount_precision = 8

	timestamp = int(time())
	data = {
		"side": type,
		"product_id": conf["products"][product],
		"price": '{:0.0{}f}'.format(float(sanitize(for_v / amount, price_precision)), price_precision),
		"size": '{:0.0{}f}'.format(float(sanitize(amount, amount_precision)), amount_precision)
	}
	req = requests.Request(
		"POST",
		conf["api"]["private"]["action-url"],
		headers={
			"CB-ACCESS-KEY": api_utils.load_key(conf["api"]["private"]["api-key-public"]),
			"CB-ACCESS-TIMESTAMP": str(timestamp),
			"CB-ACCESS-PASSPHRASE": api_utils.load_key(conf["api"]["private"]["api-key-passphrase"]),
			"User-Agent": "Mozilla/5.0 (Windows NT 5.1; rv:5.0.1) Gecko/20100101 Firefox/5.0.1",
			"Content-Type": "application/json"
		},
		data=json.dumps(data)
	).prepare()

	hash_string = "%d%s%s%s" % (
		timestamp,
		"POST",
		"/orders",
		json.dumps(data)
	)
	req.headers["CB-ACCESS-SIGN"] = base64.b64encode(hmac.new(
		str(base64.b64decode(api_utils.load_key(conf["api"]["private"]["api-key-private"]))),
		hash_string,
		digestmod=hashlib.sha256
	).digest())

	print req.body
	if conf["test_mode"]:
		return req.headers, req.body, {"test_mode": "TEST MODE"}, "TEST MODE"

	with requests.Session() as session:
		r = session.send(req)

	print r.text
	return req.headers, req.body, r.headers, r.text
