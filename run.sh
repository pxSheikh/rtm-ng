#!/bin/bash

# RTM launcher script v3

RUN_PROGRAM="python2.7 -W ignore rtm.py"

PULL=true
LOOP=false
TURBO=false
TIME=30

while getopts ":nl:t:" opt; do
	case $opt in
    	n)
			PULL=false
			;;
		l)
			LOOP=true
			TIME=$OPTARG
			;;
		t)
			TURBO=true
			TURBO_TIME=$OPTARG
			;;
		:)
			if [ "$OPTARG" == "l" ]; then
				LOOP=true
			fi
	esac
done

if [ $PULL == true ]; then
	git stash
	git pull
	git stash apply
	git stash drop
fi

if [ $LOOP == true ]; then
	while true; do
		if ! $RUN_PROGRAM ; then
			echo -e "\nTurbo mode: $TURBO_TIME seconds...\n"
			sleep $TURBO_TIME
		else
			echo -e "\nWaiting $TIME seconds...\n"
			sleep $TIME
		fi
	done
else
	$RUN_PROGRAM
fi
