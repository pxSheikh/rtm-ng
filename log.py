import sys
import sqlite3
from time import time
from datetime import datetime

from action import ActionType, TransactionType


class Log:
	def __init__(self, conf):
		self.conf = conf
		self.db = sqlite3.connect(conf["log"]["file"])
		self.db.row_factory = Log.dict_factory
		self.c = self.db.cursor()

		self.run = None

	@staticmethod
	def dict_factory(cursor, row):
		d = {}
		for idx, col in enumerate(cursor.description):
			d[col[0]] = row[idx]
		return d

	@staticmethod
	def _create_database(schema, file):
		with open(schema) as schema_file:
			schema_sql = schema_file.read()

		db = sqlite3.connect(file)
		c = db.cursor()
		c.executescript(schema_sql)

		db.commit()
		db.close()
		print "[RTM-LOG] Log database created. %s --> %s" % (schema, file)

	def start_run(self):
		self.c.execute("INSERT INTO run (timestamp) VALUES (?)", (time(),))
		self.run = self.c.lastrowid
		print "[RTM-LOG] Logging run %d." % self.run

	def finish_run(self, load_time, data_time, calc_time, action_time, total_time, active):
		self.c.execute(
			"UPDATE run SET load_time=?, data_time=?, calc_time=?, action_time=?, total_time=?, active=? WHERE id=?",
			(load_time, data_time, calc_time, action_time, total_time, active, self.run)
		)
		self.db.commit()
		self.db.close()
		print "[RTM-LOG] Run %d closed." % self.run

	def log_price(self, market, product, ask, bid):
		self.c.execute(
			"INSERT INTO prices (runid, timestamp, market, product, ask, bid) VALUES (?, ?, ?, ?, ?, ?)",
			(self.run, time(), market, product, ask, bid)
		)

	def log_balance(self, _time, market, currency, balance):
		self.c.execute(
			"INSERT INTO balances (runid, timestamp, time,  market, currency, balance) VALUES (?, ?, ?, ?, ?, ?)",
			(self.run, time(), _time, market, currency, balance)
		)

	def _atype(self, t):
		if t == ActionType.Buy:
			return "BUY"
		elif t == ActionType.Sell:
			return "SELL"

	def _ttype(self, t):
		if t == TransactionType.Direct:
			return "DIRECT"
		elif t == TransactionType.Euro:
			return "EURO"
		elif t == TransactionType.Crypto:
			return "CRYPTO"
		elif t == TransactionType.Crazy:
			return "CRAZY"

	def log_trade(self, t):
		n = datetime.now()
		self.c.execute(
			"INSERT INTO transactions \
			(runid, timestamp, date, time, type, profit, currency, normalized_profit, percent_profit, percent_base, market1, market2, currencies, actions) \
			VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
			(
				self.run,
				time(),
				"%d-%d-%d" % (n.year, n.month, n.day),
				"%d:%d:%d" % (n.hour, n.minute, n.second),
				self._ttype(t.type),
				t.profit,
				t.profit_currency,
				t.normalized_profit(),
				t.percent_profit(),
				t.actions[0].currency,
				t.actions[0].market.code(),
				t.actions[-1].market.code(),
				t.currencies_str(),
				len(t.actions)
			)
		)
		for a in t.actions:
			self.c.execute(
				"INSERT INTO trades \
				(runid, timestamp, date, time, market, product, type, amount, currency, for_amount, for_currency, response) \
				VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
				(
					self.run,
					time(),
					"%d-%d-%d" % (n.year, n.month, n.day),
					"%d:%d:%d" % (n.hour, n.minute, n.second),
					a.market.code(),
					a.product,
					self._atype(a.type),
					a.amount,
					a.currency,
					a.for_v,
					a.for_c,
					a.dump_http()
				)
			)

	def log_exchange_rates(self, mm):
		self.c.execute(
			"INSERT INTO exchange_rates (runid, timestamp, data) VALUES (?, ?, ?)",
			(self.run, time(), mm.dump_exchange_rates())
		)

	def load_exchange_rates(self):
		self.c.execute("SELECT * FROM exchange_rates ORDER BY id DESC")
		r = self.c.fetchone()
		if not r:
			print "[RTM-LOG] No exchange rates cached yet."
			return None

		if time() - r["timestamp"] <= self.conf["exchange_rates"]["ttl"]:
			ttl = self.conf["exchange_rates"]["ttl"] - (time() - r["timestamp"])
			print "[RTM-LOG] Loading cached exchange rates from run %d. TTL %ds" % (r["runid"], ttl)
			return r["data"]
		else:
			print "[RTM-LOG] Exchange rates from run %d expired." % r["runid"]
			return None


if __name__ == "__main__":
	if len(sys.argv) >= 2:
		if sys.argv[1] == "createdb":
			if len(sys.argv) >= 4:
				Log._create_database(sys.argv[2], sys.argv[3])
			else:
				print "[RTM-LOG] Error: createdb <schema> <db_path>"
	else:
		print "[RTM-LOG] No operation specified."
